/** Back to top */
let buttongoup = document.getElementById("backToTop");
window.onscroll = function () {
  scrollDown();
};
function scrollDown() {
  if (document.body.scrollUp > 20 || document.documentElement.scrollUp > 20) {
    buttongoup.style.display = "block";
  } else {
    buttongoup.style.display = "none";
  }
}

function backToTop() {
  document.body.scrollUp = 0;
  document.documentElement.scrollUp = 0;
}
