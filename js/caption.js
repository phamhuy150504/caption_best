/*
 * search-box 
 */
function searchBox() {
    var search_box = document.getElementById('search-box');
    search_box.classList.toggle('open')
    var inputSearchbox = document.getElementById('input-search-box');
    
    if (search_box.classList.contains('open')) {
        inputSearchbox.style.paddingLeft = '40px'
        inputSearchbox.style.paddingRight = '10px'
    } else {
        inputSearchbox.style.paddingLeft = '0px'
        inputSearchbox.style.paddingRight = '0px'
    }
}

// dark light
const farm = document.querySelector('.carousel-container-text h2')
const card_body = document.querySelectorAll('.service-item .card-body')
const body = document.querySelector('body');

const card_body_text_a = document.querySelectorAll('.service-item .card-body a')
document.getElementById("gg-moon").onclick = function() {
    var gg_moon = document.getElementById('gg-moon');
    gg_moon.classList.toggle('gg-sun');   
    console.log(gg_moon)  
    if (gg_moon.classList.toggle('gg-moon')) {
        body.style.background = 'white';
        body.style.color = 'black';
        body.style.transition = '.5s'
        card_body[0].style.backgroundColor = 'white'
        card_body[1].style.backgroundColor = 'white'
        card_body[2].style.backgroundColor = 'white'
        card_body_text_a[0].style.color = 'black'
        card_body_text_a[1].style.color = 'black'
        card_body_text_a[2].style.color = 'black'
        farm.style.color = '#f4f4f4'
    } else {    
        body.style.background = '#222222';
        body.style.color = 'white';
        body.style.transition = '.5s'
        card_body[0].style.backgroundColor = '#222222'
        card_body[1].style.backgroundColor = '#222222'
        card_body[2].style.backgroundColor = '#222222'
        card_body_text_a[0].style.color = 'white'
        card_body_text_a[1].style.color = 'white'
        card_body_text_a[2].style.color = 'white'
        farm.style.color = '#0b0d0e'
    }
}

// dropdown 
document.getElementById('dropdown').onclick = function() {
    var dropdown_menu = document.getElementById('dropdown-menu');
    var display = document.getElementById('dropdown-display');
    var scroll = "scroll"
    if (dropdown_menu.classList.contains(scroll)) {
        dropdown_menu.classList.remove(scroll);
    } else {
        dropdown_menu.classList.add(scroll);
        display.style.display = 'inline-block';
    }
};

// owl carousel 
$('.owl-carousel').owlCarousel({
    loop:true,
    autoplay: true,
    autoplayTimeout:5000,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})